import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
// import reportWebVitals from './reportWebVitals';

import App from './App'
// 第三方样式
import 'antd-mobile/dist/antd-mobile.css'
// 字体图标库
import './assets/fonts/iconfont.css'
// 第三方库的样式必须在全局样式之前导入，不然会覆盖全局样式
import './index.css'

// axios node-sass react-virtualized(专门用来渲染长列表以及表格数据) prop-types react-spring(js动画库)

ReactDOM.render(
	<BrowserRouter>
		<App />
	</BrowserRouter>,
	document.getElementById('root')
)

// reportWebVitals();
