// import './App.css'

// 第三方库
import 'react-virtualized/styles.css'

import { Redirect, Switch, Route } from 'react-router-dom'
import TabBar from './components/TabBar'
import Home from './pages/home/Home'
import FindHouse from './pages/findhouse/FindHouse'
import Infomation from './pages/infomation/Infomation'
import Personal from './pages/personal/Personal'
import CityList from './pages/cityList/CityList'
import MapContainer from './pages/map/MapContainer'

function App() {
	return (
		<div className="App">
			<Switch>
				<Route exact path="/" component={Home}></Route>
				<Route path="/home" component={Home}></Route>
				<Route path="/findhouse" component={FindHouse}></Route>
				<Route path="/infomation" component={Infomation}></Route>
				<Route path="/personal" component={Personal}></Route>
				<Route path="/citylist" component={CityList}></Route>
				<Route path="/map" component={MapContainer}></Route>
				<Redirect to="/"></Redirect>
			</Switch>
			<TabBar></TabBar>
		</div>
	)
}

export default App
