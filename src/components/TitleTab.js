import './tabbar.css'
export default function (props){
    return (
        <div className="titleTab">
            <h3>{props.leftText}</h3>
            <span>{props.rightText}</span>
        </div>
    )
}