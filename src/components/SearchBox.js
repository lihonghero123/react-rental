import { Flex, Icon, InputItem, List } from 'antd-mobile'
import { useEffect, useState } from 'react'
import { withRouter } from 'react-router-dom'

import { getLocalCityById } from '../utils/common'

// 通过H5API获取地理位置
// navigator.geolocation.getCurrentPosition((pos) => {
// 	console.log('当前地理位置', pos)
// })
function SearchBox(props) {
	const [city, setCity] = useState('')

	useEffect(() => {
		getLocalCityById().then((result) => {
			setCity(result.label)
		})
	}, [])
	return (
		<Flex className="searchBox">
			{/* 左侧白色区域 */}
			<Flex className="searchLeft">
				<Flex
					className="addressBox"
					onClick={() => props.history.push('/citylist')}
				>
					<p className="addressText">{city}</p>
					<Icon type="down"></Icon>
				</Flex>
				<div className="inputBox">
					<List>
						<InputItem value="dsfsd" placeholder="请输入小区或地址">
							<Icon color="#ccc" type="search" size="xs"></Icon>
						</InputItem>
					</List>
				</div>
			</Flex>
			{/* 右侧地图图标 */}
			<i
				className="iconfont icon-map"
				onClick={() => props.history.push('/map')}
			></i>
		</Flex>
	)
}
export default withRouter(SearchBox)
