import style from './listContainer.module.css'

import { baseURL } from '../api/axios'

export default function ListContainer({ id, img, title, desc, tags, price }) {
	return (
		<div className={style.house} key={id}>
			<img className={style.img} src={`${baseURL}${img}`}></img>
			<div className={style.content}>
				<h3 className={style.title}>{title}</h3>
				<div className={style.desc}>{desc}</div>
				<div>
					{tags
						? tags.map((tag, index) => {
								const classTag = 'tag' + (index + 1)
								return (
									<span
										className={[
											style.tag,
											style[classTag],
										].join(' ')}
										key={tag}
									>
										{tag}
									</span>
								)
						  })
						: null}
				</div>
				<div className={style.price}>
					<span className={style.priceNum}>{price}</span>
					元/月
				</div>
			</div>
		</div>
	)
}
