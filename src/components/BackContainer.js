import { NavBar } from 'antd-mobile'
import { withRouter } from 'react-router'
import PropTypes from 'prop-types'

import './backcontainer.sass'

BackContainer.propTypes = {
    children: PropTypes.string.isRequired,
    backWhere: PropTypes.func
}
function BackContainer({children,history,backWhere}) {
	return (
		<div className="parentNav">
			<NavBar
                className="navbarBox"
				mode="light"
				icon={<i className="iconfont icon-back"></i>}
				onLeftClick={() => history.go(-1)}
			>
				{children}
			</NavBar>
		</div>
	)
}
export default withRouter(BackContainer)
