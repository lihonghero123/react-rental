import { Component } from 'react'
import { Carousel } from 'antd-mobile'

import {getSwiperData} from '../api/home'
import {baseURL} from '../api/axios'

import SearchBox from './SearchBox'

import './tabbar.css'

export default class extends Component {
	state = {
		data: [],
		imgHeight: 212,
	}
	async componentDidMount() {
		const swiperData = await getSwiperData()
        this.setState({data: swiperData})
	}
    // 渲染轮播图
    renderSwiper(){
        return (this.state.data.map((imgItem) => (
            <a
                key={imgItem.id}
                style={{
                    display: 'inline-block',
                    width: '100%',
                    height: this.state.imgHeight,
                }}
            >
                <img
                    src={`${baseURL}${imgItem.imgSrc}`}
                    alt=""
                    style={{ width: '100%', verticalAlign: 'top' }}
                    onLoad={() => {
                        // 当窗口尺寸发生变化，图片高度也变化
                        window.dispatchEvent(new Event('resize'))
                        this.setState({ imgHeight: 'auto' })
                    }}
                />
            </a>
        )))
    }
	render() {
        const {data} = this.state
		return (
			data.length > 0 ? <div className="swiperContainer">
                {/* 搜索框 */}
                <SearchBox></SearchBox>
				<Carousel autoplay infinite>
					{this.renderSwiper()}
				</Carousel>
			</div> : ''
		)
	}
}
