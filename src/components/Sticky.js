import { useEffect, useRef } from 'react'
import styles from './listContainer.module.css'

export default function Sticky(props) {
	const ref1 = useRef()
	const ref2 = useRef()
	useEffect(() => {
		// 给内容元素绑定监听事件
		window.addEventListener('scroll', scrollFunc)
		// 该函数在组件销毁之前被执行
		return () => {
			// 清楚绑定事件
			window.removeEventListener('scroll', scrollFunc)
		}
	}, [])
	const scrollFunc = () => {
		const seize = ref1.current
		const content = ref2.current
		// 获取占位元素当前位置
		const { top } = seize.getBoundingClientRect()
		if (top < 0) {
			content.classList.add(styles.fixed) // 让内容栏变成fixed布局
			seize.style.height = '45px'
		} else {
			content.classList.remove(styles.fixed)
			seize.style.height = '0px'
		}
	}
	return (
		<div>
			{/* 占位元素 */}
			<div ref={ref1}></div>
			{/* 内容元素 */}
			<div ref={ref2}>{props.children}</div>
		</div>
	)
}
