import React from 'react'
import { TabBar } from 'antd-mobile'
import { withRouter } from 'react-router-dom'
import './tabbar.css'

// 菜单数据
const tabbarData = [
	{ title: '首页', icon: 'icon-ind', path: '/home' },
	{ title: '找房', icon: 'icon-findHouse', path: '/findhouse' },
	{ title: '资讯', icon: 'icon-infom', path: '/infomation' },
	{ title: '我的', icon: 'icon-my', path: '/personal' },
]
class TabBarExample extends React.Component {
	state = {
		selectedTab:
			this.props.location.pathname === '/'
				? '/home'
				: this.props.location.pathname,
		fullScreen: true, // 是否全屏
	}

	// 当点击HeadTab中某一项时路由地址发生，组件重新渲染，但是此时底部的TabBar因为没触发点击事件
	// 所以不会重新渲染导致底部图标不会高亮，解决办法是在钩子函数中比较前后两次路由，路由变化则改变state状态从而重新渲染
	// componentDidUpdate()有三个参数preprops，prestate,snape,上一次的props，上一次的state，以及快照
	componentDidUpdate(preprops) {
		if (preprops.location.pathname !== this.props.location.pathname) {
			this.setState({ selectedTab: this.props.location.pathname })
		}
	}

	// 渲染菜单数据
	renderTabbar() {
		return tabbarData.map((tabbarItem) => {
			return (
				<TabBar.Item
					title={tabbarItem.title}
					key={tabbarItem.title}
					icon={<i className={`iconfont ${tabbarItem.icon}`}></i>}
					selectedIcon={
						<i className={`iconfont ${tabbarItem.icon}`}></i>
					}
					selected={this.state.selectedTab === tabbarItem.path}
					onPress={() => {
						this.setState({
							selectedTab: tabbarItem.path,
						})
						this.props.history.push(tabbarItem.path)
					}}
					data-seed="logId"
				></TabBar.Item>
			)
		})
	}

	render() {
		return (
			<div
				className="tabbarBox"
				style={
					this.state.fullScreen
						? {
								position: 'fixed',
								bottom: 0,
								width: '100%',
						  }
						: { height: 400 }
				}
			>
				<TabBar
					className="tabbarStyle"
					unselectedTintColor="#949494"
					tintColor="#21b97a"
					barTintColor="white"
				>
					{this.renderTabbar()}
				</TabBar>
			</div>
		)
	}
}

export default withRouter(TabBarExample)
