import './tabbar.css'
import { Grid } from 'antd-mobile'
import { useEffect, useState } from 'react'
import {getGripData} from '../api/home'
import {baseURL} from '../api/axios'

const data1 = Array.from(new Array(4)).map(() => ({
	icon: 'https://gw.alipayobjects.com/zos/rmsportal/WXoqXTHrSnRcUwEaQgXJ.png',
}))

export default function () {
    let [gripData,setGripData] = useState([])
    useEffect(async () => {
        let data = await getGripData()
        setGripData(data)
    },[])
	return (
		<Grid
            className="gripBox"
			data={gripData}
			columnNum={2}
            square={false}
            hasLine={false}
			renderItem={(dataItem) => (
				<div className="gripItem" key={dataItem.id} style={{ padding: '12.5px' }}>
                    <div
						style={{
							color: '#888',
							fontSize: '14px',
							marginTop: '12px',
						}}
					>
						<p className="textTitle">{dataItem.title}</p>
                        <p className="textScript">{dataItem.desc}</p>
					</div>
					<img
						src={`${baseURL}${dataItem.imgSrc}`}
						style={{ width: '75px', height: '75px' }}
						alt=""
					/>
				</div>
			)}
		/>
	)
}
