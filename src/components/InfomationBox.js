import { getNewsData } from '../api/home'
import { useEffect, useState } from 'react'
import { baseURL } from '../api/axios'

import './tabbar.css'

export default function InfomationBox() {
	let [newsData, setNewsData] = useState([])
	useEffect(async () => {
		const data = await getNewsData()
		setNewsData(data)
	}, [])
	function renderNews() {
		return newsData.map((newsItem) => (
			<div className="newsBox" key={newsItem.id}>
				<img className="newsImg" src={`${baseURL}${newsItem.imgSrc}`}></img>
				<div className="newsInfoBox">
					<p className="newsTitem">{newsItem.title}</p>
					<div className="newsBottom">
						<p>{newsItem.from}</p>
						<p>{newsItem.date}</p>
					</div>
				</div>
			</div>
		))
	}
	return renderNews()
}
