import { Flex } from 'antd-mobile'
import { withRouter } from 'react-router-dom'

import './tabbar.css'

// 导入图片
import Nav1 from '../assets/images/nav-1.png'
import Nav2 from '../assets/images/nav-2.png'
import Nav3 from '../assets/images/nav-3.png'
import Nav4 from '../assets/images/nav-4.png'

const flexData = [
	{ img: Nav1, title: '整租', path: '/list' },
	{ img: Nav2, title: '合租', path: '/infomation' },
	{ img: Nav3, title: '地图找房', path: '/personal' },
	{ img: Nav4, title: '去出租', path: '/list' },
]
function FlexAredss(props) {
	function renderFlexItem() {
		return flexData.map((item) => (
			<Flex.Item key={item.title} onClick={() => props.history.push(item.path)}>
				<img src={item.img}></img>
				<h3>{item.title}</h3>
			</Flex.Item>
		))
	}
	return (
		<div className="flex-container">
			<Flex>{renderFlexItem()}</Flex>
		</div>
	)
}
export default withRouter(FlexAredss)
