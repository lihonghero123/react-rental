import { getCityInfoFromCityName } from '../api/home'

// 根据id定位城市
export const getLocalCityById = () => {
	const city = JSON.parse(localStorage.getItem('cityData'))
	if (city) {
		return Promise.resolve(city)
	} else {
		// 用一个promise把要执行的函数包裹起来，这样做的目的是可以得到回调函数的数据
		return new Promise((resolve, reject) => {
			// 通过高德地图获得位置信息
			window.AMap.plugin('AMap.CitySearch', () => {
				const citySearch = new window.AMap.CitySearch()
				citySearch.getLocalCity(async (status, result) => {
					try {
						if (status === 'complete' && result.info === 'OK') {
                            console.log(result)
							// 查询成功，result即为当前所在城市信息
							const cityData = await getCityInfoFromCityName(
								result.city
							)
							localStorage.setItem(
								'cityData',
								JSON.stringify(cityData)
							)
							resolve(cityData)
						}
					} catch (e) {
						reject(e)
					}
				})
			})
		})
	}
}
