const proxy = require('http-proxy-middleware') 
module.exports = function (app) {
    app.use(proxy('/api', {
        target: 'http://127.0.0.1:8080',
        changeOrigin: true,
        pathRewrite: { 
            '^/api': '',  //因为后端接口没有api前缀，所以这里重写（去掉前缀/api）
        }
    }))
}