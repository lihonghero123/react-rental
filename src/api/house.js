import axios from './axios'

// 根据条件查询房屋
export function getHouseData(id){
    return axios.get('/api/houses/condition',{
        params: {
            id
        }
    }).then((res)=>{
        return res.body
    }).catch((rej)=>{
        return Promise.reject(rej)
    })
}

// 根据筛选条件查询房屋列表数据
export function getHouseDataFromSelectItem(data){
    return axios.get('/api/houses',{
        params: data
    }).then((res)=>{
        return res.body
    }).catch((rej)=>{
        return Promise.reject(rej)
    })
}