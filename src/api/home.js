import axios from './axios'

// 获取轮播图数据
export function getSwiperData(){
    return axios.get('/api/home/swiper').then((res)=>{
        return res.body
    }).catch((rej)=>{
        return Promise.reject(rej)
    })
}

// 获取租房小组数据
export function getGripData(pramas) {
    return axios.get('/api/home/groups',{
        params:{
            area: 'AREA|88cff55c-aaa4-e2e0'
        }
    }).then(res => res.body).catch(rej => Promise.reject(rej))
}

// 最新资讯数据
export function getNewsData(){
    return axios.get('/api/home/news',{
        params:{
            area: 'AREA|88cff55c-aaa4-e2e0'
        }
    }).then(res => res.body).catch(rej => Promise.reject(rej))
}

// 根据城市名查询城市信息
export function getCityInfoFromCityName(city){
    return axios.get('/api/area/info',{
        params:{
            name: city
        }
    }).then(res => res.body).catch(rej => Promise.reject(rej))
}

// 获取城市列表数据
export function getCityListData(){
    return axios.get('/api/area/city',{
        params:{
            level: 1
        }
    }).then(res => res.body).catch(rej => Promise.reject(rej))
}

// 获取热门城市数据
export function getHotListData(){
    return axios.get('/api/area/hot').then(res => res.body).catch(rej => Promise.reject(rej))
}