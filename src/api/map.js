import axios from './axios'

// 根据区域id，查询该区域的房源数据
export const getHouseDataFromId = (id) => {
	return axios
		.get('/api/area/map', {
			params: {
				id,
			},
		})
		.then((res) => res.body)
		.catch((rej) => Promise.reject(rej))
}

// 根据小区id，查询该小区下的房源信息
export const getHouseList = (id) => {
	return axios
		.get('/api/houses', {
			params: {
				cityId: id
			},
		})
		.then((res) => res.body)
		.catch((rej) => Promise.reject(rej))
}