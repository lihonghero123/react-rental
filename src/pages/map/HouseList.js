import TitleTab from '../../components/TitleTab'
import ListContainer from '../../components/ListContainer'
import style from './index.module.css'

export default function HouseList(props) {
	const { houseData } = props
	return (
		<div
			className={[
				style.houselist,
				houseData.length > 0 ? style.show : '',
			].join(' ')}
		>
			<TitleTab leftText="房屋列表" rightText="更多房源"></TitleTab>
			<div className={style.houseItems}>
				{houseData.map((item) => (
					<ListContainer
                        key={item.houseCode}
						id={item.houseCode}
						img={item.houseImg}
						title={item.title}
						desc={item.desc}
						tags={item.tags}
						price={item.price}
					></ListContainer>
				))}
			</div>
		</div>
	)
}
