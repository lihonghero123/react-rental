import { useEffect, useState } from 'react'
import BackContainer from '../../components/BackContainer'
import HouseList from './HouseList'
import { getHouseDataFromId, getHouseList } from '../../api/map'

//引入地图样式
import style from './index.module.css'
import { Toast } from 'antd-mobile'
const AMap = window.AMap

export default function MapContainer() {
	let mapInfo = null
	const [houseList, setHouseData] = useState([])
	useEffect(() => {
		// 获取当前点位/点击城市
		const { label, value } = JSON.parse(localStorage.getItem('cityData'))
		AMap.plugin('AMap.Geocoder', () => {
			const geocoder = new AMap.Geocoder({
				// city 指定进行编码查询的城市，支持传入城市名、adcode 和 citycode
				city: label,
			})
			geocoder.getLocation(label, async (status, result) => {
				if (status === 'complete' && result.info === 'OK') {
					// result为对应的地理位置详细信息
					let { lng, lat } = result.geocodes[0].location
					// 初始化地图
					const map = new AMap.Map('container', {
						resizeEnable: true, //是否监控地图容器尺寸变化
						zoom: 11, //初始化地图层级
						center: [lng, lat], //初始化地图中心点
					})
					// 监听用户是否滑动地图，若滑动则隐藏房源列表
					map.on('touchstart', () => {
						setHouseData([])
					})
					mapInfo = map
					AMap.plugin(['AMap.ToolBar', 'AMap.Scale'], () => {
						// 在图面添加工具条控件，工具条控件集成了缩放、平移、定位等功能按钮在内的组合控件
						map.addControl(new AMap.ToolBar())
						// 在图面添加比例尺控件，展示地图在当前层级和纬度下的比例尺
						map.addControl(new AMap.Scale())
					})
					renderMarker(value)
				}
			})
		})
	}, [])

	// 渲染覆盖物入口
	// 接收区域id，获取该id下的房源信息，获取房源类型和地图缩放级别
	const renderMarker = async (id) => {
		Toast.loading('加载中...', 0, null, false)
		const houseData = await getHouseDataFromId(id) // 获取该城市房源信息
		Toast.hide()
		const { type, nextZoom } = computeTypeAndZoom()
		houseData.forEach((item) => {
			createMarkerFromType(item, type, nextZoom)
		})
	}

	/**
	 * 计算要绘制的覆盖物类型以及下一个缩放级别
	 * 区：11 范围 >=10 <12
	 * 镇  13 范围 >=12 <14
	 * 小区 15 范围 >= 14 <16
	 */
	const computeTypeAndZoom = () => {
		const zoom = mapInfo.getZoom()
		let type = ''
		let nextZoom = null
		if (zoom >= 10 && zoom < 12) {
			// 区
			nextZoom = 13 //下一个缩放级别
			type = 'circle' //表示设置圆形覆盖物
		} else if (zoom >= 12 && zoom < 14) {
			nextZoom = 15
			type = 'circle'
		} else if (zoom >= 14 && zoom < 16) {
			type = 'rect'
		}
		return { type, nextZoom }
	}
	// 根据type类型创建不同的覆盖物
	const createMarkerFromType = (data, type, nextZoom) => {
		if (type === 'circle') {
			// 说明是区或者镇
			createCircle(data, nextZoom)
		} else {
			// 小区
			createRect(data)
		}
	}
	// 创建区或者镇覆盖物
	const createCircle = (data, nextZoom) => {
		const {
			coord: { longitude, latitude },
		} = data
		let marker = new AMap.Text({
			position: [longitude, latitude], // 经纬度对象，也可以是经纬度构成的一维数组[116.39, 39.9]
			offset: new AMap.Pixel(-35, -35),
		})
		let content = `<div class=${style.contentBox} key=${data.value}>
                    <p class=${style.name}>${data.label}</p>
                    <p class=${style.num}>${data.count}套</p>
                </div>`
		marker.setContent(content)
		// 为每一个覆盖物添加单击事件
		marker.on('click', () => {
			// 以当前点击的覆盖物为中心放大地图
			mapInfo.setZoomAndCenter(nextZoom, [longitude, latitude])
			// 使用clearMap方法删除所有覆盖物
			mapInfo.clearMap()
			// 调用renderMarker()渲染当前区下的镇的房源数据 xx区 -> xx镇
			renderMarker(data.value)
		})
		// 将创建的点标记添加到已有的地图实例
		mapInfo.add(marker)
	}
	// 创建小区覆盖物
	const createRect = (data) => {
		const {
			coord: { longitude, latitude },
		} = data
		let marker = new AMap.Text({
			position: [longitude, latitude], // 经纬度对象，也可以是经纬度构成的一维数组[116.39, 39.9]
			offset: new AMap.Pixel(-50, -28),
		})
		let content = `<div class=${style.rectBox} key=${data.value}>
                    <span class=${style.housename}>${data.label}</span>
                    <span class=${style.housenum}>${data.count}套</span>
                    <i class=${style.arrow}></i>
                </div>`
		marker.setContent(content)
		// 为每一个覆盖物添加单击事件
		marker.on('click', async (e) => {
			Toast.loading('加载中...', 0, null, false)
			const houseList = await getHouseList(data.value)
			Toast.hide()
			// 移动地图到视图中心
			// console.log(e.originEvent)
			const target = e.originEvent
			mapInfo.panBy(
				window.innerWidth / 2 - target.clientX,
				(window.innerHeight - 330) / 2 - target.clientY
			)
			setHouseData(houseList.list)
		})
		// 将创建的点标记添加到已有的地图实例
		mapInfo.add(marker)
	}
	return (
		<div className={style.map}>
			<BackContainer staticStyle="margin-top: -45px">
				地图找房
			</BackContainer>
			<div id="container" className={style.container}></div>
			<HouseList houseData={houseList}></HouseList>
		</div>
	)
}
