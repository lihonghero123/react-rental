import SwiperBox from '../../components/SwiperBox'
import HeadTab from '../../components/HeadTab'
import TitleTab from '../../components/TitleTab'
import GripTab from '../../components/GripTab'
import InfomationBox from '../../components/InfomationBox'
import './home.css'
export default function Home(){
    return (<div className="home">
        {/* 轮播图 */}
        <SwiperBox></SwiperBox>
        {/* 导航栏 */}
        <HeadTab></HeadTab>
        {/* 标题栏 */}
        <TitleTab leftText="租房小组" rightText="更多"></TitleTab>
        {/* 导航宫格 */}
        <GripTab></GripTab>
        {/* 标题栏 */}
        <TitleTab leftText="最新资讯" rightText=""></TitleTab>
        {/* 资讯栏 */}
        <InfomationBox></InfomationBox>
    </div>)
}
