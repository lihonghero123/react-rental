import style from './filterMore.module.css'

import ClickBtn from '../ClickBtn/ClickBtn'
import { useState } from 'react'

export default function FilterMore({ houseData,setSelectItem,selectFilterMoreValue,checkIsShow }) {
	const { roomType, characteristic, floor, oriented } = houseData
	let [maskStatus, setMaskStatus] = useState(true)
	let [selectValue, setSelectValue] = useState(selectFilterMoreValue) // 选中项的数组
	// 取消/遮罩层关闭遮罩层
	const hideMask = () => {
		setMaskStatus(false)
        setSelectValue([])
        checkIsShow('')
	}
	// 点击确定关闭遮罩层
	const okHideMask = () => {
		setMaskStatus(false)
        setSelectItem(selectValue)
        checkIsShow('')
	}

	const getSelectItem = (value) => {
		if (selectValue.includes(value)) {
			const filterAll = selectValue.filter(item => value !== item)
            setSelectValue(filterAll)
		}else{
            setSelectValue([...selectValue, value])
        }
	}
	return (
		<>
			{maskStatus ? (
				<div className={style.mask} onClick={hideMask}></div>
			) : null}
			<dl className={style.filterMore}>
				<dt className={style.title}>户型</dt>
				<div className={style.ddItem}>
					{roomType.map((item) => {
						const isSelect = selectValue.includes(item.value)
						return (
							<dd
								key={item.value}
								className={[
									style.item,
									isSelect ? style.selectStyle : '',
								].join(' ')}
								onClick={() => {
									getSelectItem(item.value)
								}}
							>
								{item.label}
							</dd>
						)
					})}
				</div>
				<dt className={style.title}>朝向</dt>
				<div className={style.ddItem}>
					{oriented.map((item) => (
						<dd key={item.value} className={style.item}>
							{item.label}
						</dd>
					))}
				</div>
				<dt className={style.title}>楼层</dt>
				<div className={style.ddItem}>
					{floor.map((item) => (
						<dd key={item.value} className={style.item}>
							{item.label}
						</dd>
					))}
				</div>
				<dt className={style.title}>房屋亮点</dt>
				<div className={style.ddItem}>
					{characteristic.map((item) => (
						<dd key={item.value} className={style.item}>
							{item.label}
						</dd>
					))}
				</div>
				<ClickBtn
					className={style.click}
					hideMask={hideMask}
					okHideMask={okHideMask}
				></ClickBtn>
			</dl>
		</>
	)
}
