import { PickerView, WhiteSpace } from 'antd-mobile'
import { useState } from 'react'

import ClickBtn from '../ClickBtn/ClickBtn'

export default function FilterPicker({
	hideMask,
	okHideMask,
	houseData,
	row,
	type,
	defaultSelectedValue,
}) {
	let [value, setValue] = useState(defaultSelectedValue)
	return (
		// value表示选中项 如果为null，该组件会一直在默认项
		<div style={{ backgroundColor: '#fff' }}>
			<PickerView
				data={houseData}
				value={value}
				cols={row}
				onChange={(val) => setValue(val)}
			/>
			<ClickBtn
				hideMask={hideMask}
				okHideMask={() => okHideMask(type, value)}
			></ClickBtn>
		</div>
	)
}
