import style from './filterTitle.module.css'

// 租房标题
const houseTitle = [
	{ type: 'area', value: '区域' },
	{ type: 'mode', value: '方式' },
	{ type: 'price', value: '租金' },
	{ type: 'more', value: '筛选' },
]
export default function FilterTitle({ highlightStatus, onChangeStatus }) {
	return (
		<div
			className={[
				style.filterTitle,
				highlightStatus.more ? style.filterIndex : '',
			].join(' ')}
		>
			{houseTitle.map((houseItem) => {
				const selectType = highlightStatus[houseItem.type]
				return (
					<div
						className={[style.filterContent]}
						key={houseItem.type}
						onClick={() => onChangeStatus(houseItem)}
					>
						<p
							className={[
								style.unSelect,
								selectType ? style.title : '',
							].join(' ')}
						>
							{houseItem.value}
						</p>
						<div
							className={[
								style.chart,
								selectType
									? style.selectChart
									: style.unSelectChart,
							].join(' ')}
						></div>
					</div>
				)
			})}
		</div>
	)
}
