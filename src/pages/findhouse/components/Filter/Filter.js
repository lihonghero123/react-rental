import { useEffect, useState } from 'react'
import { Spring, animated } from 'react-spring'
import FilterMore from '../FilterMore/FilterMore'
import FilterPicker from '../FilterPicker/FilterPicker'
import FilterTitle from '../FilterTitle/FilterTitle'
import { getHouseData, getHouseDataFromSelectItem } from '../../../../api/house'

import style from './filter.module.css'

export default function Filter({ getFilterHouseList }) {
	const { value } = JSON.parse(localStorage.getItem('cityData'))
	// 高亮状态,默认都没选中，不高亮
	let [highlightStatus, setHighStatus] = useState({
		area: false,
		mode: false,
		price: false,
		more: false,
	})
	let [isShow, setShow] = useState('')
	let [allHouseData, setHouseData] = useState({})
	let [selectedValue, setSelectedValue] = useState({
		area: ['area', 'null'],
		mode: ['null'],
		price: ['null'],
		more: [],
	})
	let [filterMoreValue, setFilterMore] = useState([]) // filterMore组件选中项数据
	// 点击导航标题
	const onChangeStatus = ({ type }) => {
		document.body.style.overflow = 'hidden' // 让弹出的模态框不可滚动
		let copyHighLightArr = { ...highlightStatus } //复制高亮数组
		Object.keys(highlightStatus).forEach((key) => {
			// 当前标题 让当前选中标题高亮
			if (type === key) {
				copyHighLightArr[key] = true
				return
			}
			// 其他标题
			const selectValue = selectedValue[key] // 选中数据数组
			if (
				key === 'area' &&
				(selectValue.length !== 2 || selectValue[0] !== 'area')
			) {
				copyHighLightArr[key] = true
			} else if (key === 'mode' && selectValue[0] !== 'null') {
				copyHighLightArr[key] = true
			} else if (key === 'price' && selectValue[0] !== 'null') {
				copyHighLightArr[key] = true
			} else if (key === 'more' && selectValue[0] !== 'null') {
			} else {
				copyHighLightArr[key] = false
			}
		})
		setHighStatus(copyHighLightArr)
		// 当isShow的值不为''时，有遮罩层有对应组件
		setShow(type)
	}
	// 取消/遮罩层关闭遮罩层
	const hideMask = () => {
		setShow('')
		document.body.style.overflow = 'auto' // 关闭模态框让弹出的模态框可滚动
	}
	// 点击确定关闭遮罩层
	let filterData = {} // 筛选传递给后台的数据
	filterData.cityId = value // 把城市id赋给筛选数组
	const okHideMask = async (type, value) => {
		let newSelectedValue = { ...selectedValue, [type]: value } // 最新值
		const { area, mode, price, more } = newSelectedValue // 清洗数据
		// 区域
		const areaKey = area[0]
		let areaValue = 'null'
		if (area.length === 3) {
			areaValue = area[2] === 'null' ? area[1] : area[2]
		}
		filterData[areaKey] = areaValue
		// 方式和租金
		filterData.mode = mode[0]
		filterData.price = price[0]
		// 更多筛选条件
		filterData.more = more.join(',')
		setSelectedValue(newSelectedValue)
		setShow('')
		const houseList = await getHouseDataFromSelectItem(filterData)
		getFilterHouseList(houseList)
		document.body.style.overflow = 'auto'
	}
	useEffect(() => {
		async function getHouseFromAjax() {
			// 得到导航栏数据（area，mode，price，more）
			const house = await getHouseData(value)
			setHouseData(house)
		}
		getHouseFromAjax()
	}, [])
	// 渲染filterPicker组件
	const renderFilterPicker = () => {
		if (isShow === 'area' || isShow === 'mode' || isShow === 'price') {
			const { area, price, rentType, subway } = allHouseData
			let data = []
			let row = 3
			const defaultSelectedValue = selectedValue[isShow] // 选中默认值
			switch (isShow) {
				case 'area':
					// 区域
					data = [area, subway]
					break
				case 'mode':
					// 模式
					data = rentType
					row = 1
					break
				case 'price':
					// 租金
					data = price
					row = 1
					break
				default:
					break
			}
			return (
				<FilterPicker
					key={isShow}
					okHideMask={okHideMask}
					hideMask={hideMask}
					houseData={data}
					row={row}
					type={isShow}
					defaultSelectedValue={defaultSelectedValue}
				></FilterPicker>
			)
		}
		return null
	}
	// 渲染FilterMore组件
	const renderFilterMore = () => {
		if (isShow === 'more') {
			return (
				<FilterMore
					houseData={allHouseData}
					checkIsShow={(flag) => setShow(flag)}
					selectFilterMoreValue={filterMoreValue}
					setSelectItem={(data) => {
						getFilterMoreData(data)
					}}
				></FilterMore>
			)
		}
	}
	// filterMore组件传递过来的数据
	const getFilterMoreData = (data) => {
		okHideMask('more', data)
		setFilterMore(data)
	}
	// 渲染遮罩层
	const renderMask = () => {
		const isHide = isShow === 'more' || isShow === ''
		// from={{ opacity: 0 }} to={{ opacity: isHide ? 0 : 1 }}
		return (
			<Spring opacity={isHide ? 0 : 1}>
				{(props) => {
					return (
						<animated.div
							style={props}
							className={[
								isHide ? '' : style.mask,
								isHide ? '' : style.maskColor,
							].join(' ')}
							onClick={hideMask}
						></animated.div>
					)
				}}
			</Spring>
		)
	}
	return (
		<div className={style.filterRoot}>
			{/* 遮罩层 */}
			{renderMask()}
			<div className={style.content}>
				<FilterTitle
					highlightStatus={highlightStatus}
					onChangeStatus={onChangeStatus}
				></FilterTitle>
				{renderFilterPicker()}
				{renderFilterMore()}
			</div>
		</div>
	)
}
