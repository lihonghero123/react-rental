import style from './click.module.css'

import PropTypes from 'prop-types'

ClickBtn.prototype = {
	className: PropTypes.string,
    hideMask: PropTypes.func
}
export default function ClickBtn({ className,hideMask,okHideMask }) {
	return (
		<div
			className={[style.btnContent, className ? className : ''].join(' ')}
		>
			<div className={style.cancel} onClick={hideMask}>取消</div>
			<div className={style.determine} onClick={okHideMask}>确定</div>
		</div>
	)
}
