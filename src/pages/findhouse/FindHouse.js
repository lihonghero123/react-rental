import { Flex, Toast } from 'antd-mobile'
import { withRouter } from 'react-router-dom'
import SearchBox from '../../components/SearchBox'
import Filter from './components/Filter/Filter'

import { getHouseDataFromSelectItem } from '../../api/house'
import { useState, useEffect } from 'react'
// 引用第三方库渲染长列表
import {
	List,
	AutoSizer,
	WindowScroller,
	InfiniteLoader,
} from 'react-virtualized'
import ListContainer from '../../components/ListContainer'

import styles from './findHouse.module.css'
import Sticky from '../../components/Sticky'

function FindHouse(props) {
	const { value } = JSON.parse(localStorage.getItem('cityData'))
	let [allHouseList, setAllHouseList] = useState({ list: [], count: 0 }) //list是房屋数组，count是总条数
	let filterData = {} // 筛选传递给后台的数据
	filterData.cityId = value // 把城市id赋给筛选数组
	useEffect(() => {
		Toast.loading('加载中...', 0, null, false)
		getHouseDataFromSelectItem(filterData)
			.then((data) => {
				Toast.hide()
				if (data.count !== 0) {
					Toast.info(`共找到${data.count}套房源`, 2, null, false)
				}
				setAllHouseList(data)
			})
			.catch((err) => {
				console.log(err)
			})
	}, [])
	// 得到子组件传递过来的查询房屋数据
	const saveFilterHouseList = (data) => {
		setAllHouseList(data)
		// 让页面滚动到顶部
		window.scrollTo(0, 0)
	}
	// isScrolling是否在滚动 isVisible当前项在list中是可见的 style样式，必须传
	const renderItem = ({ index, key, style, isScrolling, isVisible }) => {
		const item = allHouseList.list[index]
		if (!item) {
			// 不存在，就渲染占位符
			return (
				<div key={key} style={style}>
					<p className={styles.loading}></p>
				</div>
			)
		}
		return (
			<div key={key} style={style} className="city">
				{
					<ListContainer
						style={style}
						key={item.houseCode}
						id={item.houseCode}
						img={item.houseImg}
						title={item.title}
						desc={item.desc}
						tags={item.tags}
						price={item.price}
					></ListContainer>
				}
			</div>
		)
	}
	// 每一项是否加载完成
	const isRowLoaded = ({ index }) => {
		return !!allHouseList.list[index]
	}
	// 加载更多
	const loadMoreRows = ({ startIndex, stopIndex }) => {
		filterData.start = startIndex
		filterData.end = stopIndex
		return new Promise((res) => {
			getHouseDataFromSelectItem(filterData)
				.then((data) => {
					setAllHouseList({
						list: [...allHouseList.list, ...data.list],
						count: data.count,
					})
					res()
				})
				.catch((err) => {
					console.log(err)
				})
		})
	}
	return (
		<div className={styles.findHouse}>
			<Flex className={styles.header}>
				<i
					className="iconfont icon-back"
					onClick={() => props.history.go(-1)}
				/>
				<SearchBox></SearchBox>
			</Flex>
			{/* 吸顶组件 */}
			<Sticky>
				<Filter
					getFilterHouseList={(data) => saveFilterHouseList(data)}
				></Filter>
			</Sticky>
			{/* 房屋列表 */}
			<div>
				{/* loadMoreRows 加载更多 isRowLoaded 每一列是否加载完成 */}
				<InfiniteLoader
					isRowLoaded={isRowLoaded}
					loadMoreRows={loadMoreRows}
					rowCount={allHouseList.count}
				>
					{({ onRowsRendered, registerChild }) => (
						<WindowScroller>
							{({ height, isScrolling, scrollTop }) => (
								<AutoSizer>
									{({ width }) => (
										<List
											onRowsRendered={onRowsRendered} //
											ref={registerChild} // 每一个节点元素
											autoHeight // 设置高度为windowScroller最终渲染的列表高度（内容的高）
											isScrolling={isScrolling}
											scrollTop={scrollTop}
											rowCount={allHouseList.count} //列表项的行数
											rowHeight={120} //每一行行高
											rowRenderer={renderItem} //每一行的渲染函数
											width={width} // 视图宽度
											height={height} // 视图高度
										/>
									)}
								</AutoSizer>
							)}
						</WindowScroller>
					)}
				</InfiniteLoader>
			</div>
		</div>
	)
}

export default withRouter(FindHouse)
