import { withRouter } from 'react-router'
import BackContainer from '../../components/BackContainer'

import { getCityListData, getHotListData } from '../../api/home'
import { getLocalCityById } from '../../utils/common'
import { useEffect, useRef, useState } from 'react'

// 引用第三方库渲染长列表
import { List, AutoSizer } from 'react-virtualized'

import './citylist.css'
import { Toast } from 'antd-mobile'

function CityList(props) {
	// 返回的数据格式：[{label:'北京','value':'','pinyin':'beijing','short':'bj'}]
	/**
	 * 期望的数据格式,按字母进行分类以及排序
	 *  {a:[{},{}],b:[]}
	 *  ['a','b']
	 */
	let [resultData, setResultData] = useState({})
	let [arrData, setArrData] = useState([])
	let [activeIndex, setIndex] = useState(0)
	const cityRef = useRef()
	let obj = {}
	let arr = []
	const titleH = 36 //城市标题高度
	const cityItemH = 50 //每一个城市高度
	const citySource = ['北京', '上海', '广州', '深圳']

	// 处理渲染数据
	const renderDataChange = (letter) => {
		switch (letter) {
			case '#':
				return '当前城市'
			case 'hot':
				return '热门城市'
			default:
				return letter?.toUpperCase()
		}
	}

	//  动态设置每一列城市高度
	const cityItemHeighe = ({ index }) => {
		const cityitem = resultData[arrData[index]].length //得到每一个列表所有城市总数
		return titleH + cityItemH * cityitem
	}

	// 渲染右边城市列表索引
	const cityIndex = () => {
		return arrData.map((item, index) => (
			<li
				className="city-lisy-item"
				key={item}
				onClick={() => {
					// 调用scrollToRow函数并设置scrollToAlignment都只能跳转到显示过的列表字母开头位置，如果未显示过则跳转不到
					// 要解决这个问题，需要调用measureAllRows()提前计算每一行高度
					cityRef.current.scrollToRow(index)
					setIndex(index)
				}}
			>
				<span className={activeIndex === index ? 'index-active' : ''}>
					{item === 'hot' ? '热' : item.toUpperCase()}
				</span>
			</li>
		))
	}

	// 当滚动时动态渲染右侧列表索引
	const renderScroll = ({ startIndex }) => {
		// 当滚动到下一个索引值时，才更新状态
		if (startIndex !== activeIndex) {
			setIndex(startIndex)
		}
	}

	// isScrolling是否在滚动 isVisible当前项在list中是可见的 style样式，必须传
	const renderItem = ({ index, key, style, isScrolling, isVisible }) => {
		let cityItem = arrData[index]
		return (
			<div key={key} style={style} className="city">
				<div className="title">{renderDataChange(cityItem)}</div>
				{resultData[cityItem]?.map((item) => (
					<div
						className="name"
						key={item.value}
						onClick={() => checkIsCity(item)}
					>
						{item.label}
					</div>
				))}
			</div>
		)
	}

	// 检查点击的城市是否有房源数据
	const checkIsCity = (item) => {
		if (citySource.includes(item.label)) {
			localStorage.setItem(
				'cityData',
				JSON.stringify({ label: item.label, value: item.value })
			)
			props.history.go(-1)
		} else {
			Toast.info('暂无房源', 1, null, false)
		}
	}

	useEffect(() => {
		async function fetchCityList() {
			const data = await getCityListData()
			// 判断对象中是否有某属性，有则push，没有则设置
			data.forEach((element) => {
				let frist = element.short.substr(0, 1)
				if (obj[frist]) {
					obj[frist].push(element)
				} else {
					obj[frist] = [element]
				}
			})
			arr = Object.keys(obj).sort()
			const hot = await getHotListData()
			getLocalCityById().then((result) => {
				obj['#'] = [result]
				obj['hot'] = hot
				arr.unshift('hot')
				arr.unshift('#')
				setResultData(obj)
				setArrData(arr)
				cityRef.current.measureAllRows() //提前计算每一行高度
			})
		}
		fetchCityList()
	}, [])
	return (
		// 设置根元素的高度为100%,使列表能撑起整个屏幕
		<div className="cityListBox" style={{ height: '100%' }}>
			<BackContainer>城市选择</BackContainer>
			<AutoSizer>
				{({ width, height }) => (
					<List
						ref={cityRef}
						rowCount={arrData.length}
						rowHeight={cityItemHeighe}
						rowRenderer={renderItem}
						onRowsRendered={renderScroll}
						width={width}
						height={height}
						scrollToAlignment="start"
					/>
				)}
			</AutoSizer>
			{/* 右侧城市索引列表 */}
			<ul className="city-list">{cityIndex()}</ul>
		</div>
	)
}
export default withRouter(CityList)
